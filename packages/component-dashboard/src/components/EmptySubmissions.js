import React from 'react'
import classes from './EmptySubmissions.local.scss'

const EmptySubmissions = () => (
  <div className={classes.root}>
    <div>You haven&apos;t submitted any manuscripts yet.</div>
  </div>
)

export default EmptySubmissions
