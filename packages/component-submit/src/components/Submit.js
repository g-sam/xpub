import React from 'react'
import classnames from 'classnames'
import { Link } from 'react-router-dom'
import { Button } from '@pubsweet/ui'
import Metadata from './Metadata'
import Declarations from './Declarations'
import Suggestions from './Suggestions'
import Notes from './Notes'
import SupplementaryFiles from './SupplementaryFiles'
import Confirm from './Confirm'
// import Validots from './Validots'
import classes from './Submit.local.scss'

const Submit = ({
  project,
  version,
  valid,
  error,
  readonly,
  handleSubmit,
  uploadFile,
  confirming,
  toggleConfirming,
}) => (
  <div
    className={classnames(classes.root, {
      [classes.confirming]: confirming,
    })}
  >
    <div className={classes.title}>Submission information</div>

    <div className={classes.intro}>
      <div>
        We have ingested your manuscript. To access your manuscript in an
        editor, please{' '}
        <Link to={`/projects/${project.id}/versions/${version.id}/manuscript`}>
          view here
        </Link>.
      </div>
      <div>
        To complete your submission, please answer the following questions.
      </div>
      <div>The answers will be automatically saved.</div>
    </div>

    <form onSubmit={handleSubmit}>
      <Metadata readonly={readonly} />
      <Declarations readonly={readonly} />
      <Suggestions readonly={readonly} />
      <Notes readonly={readonly} />
      <SupplementaryFiles readonly={readonly} uploadFile={uploadFile} />

      {!readonly && (
        <div>
          <Button onClick={toggleConfirming} primary type="button">
            Submit your manuscript
          </Button>
        </div>
      )}

      {confirming && (
        <div className={classes.confirm}>
          <Confirm toggleConfirming={toggleConfirming} />
        </div>
      )}
    </form>

    {/* <div className={classes.validots}>
      <Validots
        valid={valid}
        handleSubmit={handleSubmit}/>
    </div> */}
  </div>
)

export default Submit
