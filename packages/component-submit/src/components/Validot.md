A dot representing the validation state of a form field.

### Idle

```js
<Validot meta={{ valid: false }}/>
```

### Warning

```js
<Validot meta={{ valid: true, warning: 'There was a warning' }}/>
```

### Error

```js
<Validot meta={{ valid: false, error: 'There was an error' }}/>
```

### Valid

```js
<Validot meta={{ valid: true }}/>
```
