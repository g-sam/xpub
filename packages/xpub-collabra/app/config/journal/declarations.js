export default {
  questions: [
    {
      id: 'openData',
      legend: 'Data is open',
    },
    {
      id: 'previouslySubmitted',
      legend: 'Previously submitted',
    },
    {
      id: 'openPeerReview',
      legend: 'Open peer review',
    },
    {
      id: 'streamlinedReview',
      legend: 'Streamlined review',
    },
    {
      id: 'researchNexus',
      legend: 'Submitted as part of the research nexus?',
    },
    {
      id: 'preregistered',
      legend: 'Pre-registered?',
    },
  ],
}
